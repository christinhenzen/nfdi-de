![Nationale Forschungsdateninfrastruktur (NFDI)](https://gitlab.com/nfdi-de/nfdi-de/-/raw/main/images/nfdi-logo_600px.png)

# Welcome to NFDI on GitLab!

### NFDI consortia on GitLab.com
* [NFDI4Cat](https://gitlab.com/nfdi4cat)
* [NFDI4DataScience](https://gitlab.com/nfdi4ds)

### NFDI consortia on institutional GitLab Instances
* [NFDI4Ing](https://git.rwth-aachen.de/nfdi4ing)
* [NFDI4Plants](https://git.nfdi4plants.org/explore)
* [FAIRmat](https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-FAIR)
* [Text+](https://gitlab.gwdg.de/textplus)
* [PUNCH4NFDI](https://gitlab-p4n.aip.de/public)

### see also
* [NFDI on GitHub](https://github.com/nfdi-de) 
